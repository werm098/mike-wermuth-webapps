import os
import jinja2
import webapp2
from mwEntities import Quote
from mwEntities import Category
from mwEntities import quoteExists
from mwEntities import getCategoryKey
from google.appengine.ext import ndb
from google.appengine.api import users

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)
    
adminIDs = ['185804764220139124118', '106916172306015446665']

class QuoteDeleteHandler(webapp2.RequestHandler):
    def get(self):
        try:
            user = users.get_current_user()
            if user.user_id() in adminIDs or user.user_id() == oldQuote.userID:
                messageText = "You do not have permissions to delete that."
            else:
                quoteKey = self.request.get("key")
                quoteKey = ndb.Key(urlsafe=quoteKey)
                quoteKey.delete();
                messageText = "The quote was deleted."
        except:
            messageText = "There was an error editing the quote."
        
        template = JINJA_ENVIRONMENT.get_template('quoteSubmit.html')
        self.response.write(template.render({'messageText': messageText}))


class QuoteEditHandler(webapp2.RequestHandler):
    def post(self):
        try:
            quoteKey = self.request.get("key")
            quoteKey = ndb.Key(urlsafe=quoteKey)
        except:
            messageText = "There was an error editing the quote."
        
        user = users.get_current_user()
        oldQuote = quoteKey.get()
        if user.user_id() not in adminIDs and user.user_id() != oldQuote.userID: 
            messageText = "You do not have permission to edit that quote"            
        else:       
            author = self.request.get("author")
            quote = self.request.get("quote")
            messageText = "Error"
            if not author or not quote:
                messageText = "You are missing the quote or author text."
            else:
                category = self.request.get("category")
                try:
                    categoryKey = getCategoryKey(category)
                    quoteKey.delete();
                    newQuote = Quote(quote=quote,
                                     parent=categoryKey,
                                     userID=user.user_id(),
                                     author=author)
                    newQuote.put()
                    messageText = "You edited the quote '%s' by the author '%s' at (%s)" % (quote, author, newQuote.createdTime.strftime("%m-%d-%y %H:%M"))
                except:
                    messageText = "That category is not in the database."
        template = JINJA_ENVIRONMENT.get_template('quoteSubmit.html')
        self.response.write(template.render({'messageText': messageText}))

class QuoteSubmitHandler(webapp2.RequestHandler):
    def post(self):
        author = self.request.get("author")
        quote = self.request.get("quote")
        messageText = "Error"
        if not author or not quote:
            messageText = "You are missing the quote or author text."
        else:
            if quoteExists(quote):
                messageText = "That quote is already in the database."
            else:
                category = self.request.get("category")
                try:
                    if category == "create":
                        category = self.request.get("newCategory")
                        newCategory = Category(name=category)
                        categoryKey = newCategory.put()
                    else:
                        categoryKey = getCategoryKey(category)
                    user = users.get_current_user()
                    newQuote = Quote(quote=quote,
                                     parent=categoryKey,
                                     userID=user.user_id(),
                                     author=author)
                    newQuote.put()
                    messageText = "You added the quote '%s' by the author '%s' at (%s)" % (quote, author, newQuote.createdTime.strftime("%m-%d-%y %H:%M"))
                except:
                    messageText = "That category is not in the database."
        template = JINJA_ENVIRONMENT.get_template('quoteSubmit.html')
        self.response.write(template.render({'messageText': messageText}))

