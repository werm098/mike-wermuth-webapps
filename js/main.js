$(function() {
    $(".editCancelButton").click(function() {
        var form = $(this).parent();
        form.css("display", "none");
        var formButtons = $(this).parent().parent().find(".formButtons");
        formButtons.css("display", "inline-block");
        var row = $(this).parent().parent().parent();
        $(row).find(".editableCell").each(function() {
            $(this).find(".originalText").css("display", "inline-block");
            $(this).find(".formInput").css("display", "none");
        });
    });
    
    $("#categorySelect").change(function() {
        if ($(this).val() == "create") {
            $("#createCategory").css("display", "block");
        } else {
            $("#createCategory").css("display", "none");
        }
    });
    
    $(".quoteEditButton").click(function() {
        var form = $(this).parent().parent().find("form");
        form.css("display", "inline-block");
        var formButtons = $(this).parent();
        formButtons.css("display", "none");
        var row = $(this).parent().parent().parent();
        var hasInputs = $(row).find(".formInput").length > 0;
        $(row).find(".inputEditableCell").each(function() {
            var originalTextCell = $(this).find(".originalText");
            originalTextCell.css("display", "none");
            if (hasInputs) {
                $(this).find(".formInput").css("display", "inline-block");
            } else {
                var text = originalTextCell.html().trim();
                $(this).append('<div class="formInput"><input value="' + text + '" /></div>');
            }
        });
        var selectorHtml = $("#categorySelectContainerJS").html();
        $(row).find(".categoryCell").each(function() {
            var originalTextCell = $(this).find(".originalText");
            originalTextCell.css("display", "none");
            var hasInputs = $(this).find(".formInput").length > 0;
            if (hasInputs) {
                $(this).find(".formInput").css("display", "inline-block");
            } else {
                $(this).append('<div class="formInput">' + selectorHtml + '</div>');
                var text = originalTextCell.html().trim();
                $(this).find('select').val(text);
            }
        });
    });
    
    $(".editSubmitButton").click(function() {
        var row = $(this).parent().parent().parent();
        var form = $(this).parent();
        form.find("[name='quote']").val(row.find(".quoteCell input").val().trim());
        form.find("[name='author']").val(row.find(".authorCell input").val().trim());
        form.find("[name='category']").val(row.find(".categoryCell select").val().trim());
        form.submit();
    });

});