import os
import jinja2
import webapp2
import datetime
from google.appengine.api import users

import mwEntities
import quoteHandler

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)
    
adminIDs = ['185804764220139124118', '106916172306015446665']

class AddCookieHandler(webapp2.RequestHandler):
    def get(self, key):
        try:
            cookie = self.request.cookies.get('hidden').split(',')
        except:
            cookie = []
        cookie.append(key)
        self.response.set_cookie('hidden', ','.join(cookie))
        self.redirect('/')

class ClearCookieHandler(webapp2.RequestHandler):
    def get(self):        
        self.response.set_cookie('hidden', '')
        self.redirect('/')

class MainHandler(webapp2.RequestHandler):
    def get(self, type):        
        user = users.get_current_user()
        if user:
            greeting = ('Welcome, %s! (<a href="%s">sign out</a>)' %
                        (user.nickname(), users.create_logout_url('/')))
        else:
            greeting = ('<a href="%s">Sign in or register to add quotes</a>.' %
                        users.create_login_url('/'))

        self.response.out.write('%s' % greeting)
        
        sortType = self.request.get('sort')
        orderType = self.request.get('order')
        quotes = mwEntities.Quote.allQuotesWithOrder(sortType, orderType)
        if type == "me":
            quotes = mwEntities.Quote.allQuotesWithOrder(sortType, orderType, user.user_id())
        try:
            numberOfResults = quotes.count()
        except:
            numberOfResults = len(quotes)
        try:
            cookie = self.request.cookies.get('hidden').split(',')
        except:
            cookie = []
        quoteOfTheDay = mwEntities.QuoteOfTheDay.getDailyQuote()
        quoteOfTheDay = "%s - %s" % (quoteOfTheDay.quote, quoteOfTheDay.author)
        template_values = {
            'user': user,
            'adminIDs': adminIDs,
            'categories': mwEntities.Category.allCategories(),
            'quotes':  quotes,
            'type':  type,
            'cookie':  cookie,
            'quoteOfTheDay': quoteOfTheDay,
            'numberOfResults': numberOfResults,
            'order':  "up" if orderType=="down" else "down"
        }
        template = JINJA_ENVIRONMENT.get_template('mainTemplate.html')
        self.response.write(template.render(template_values))
        
class AdminHandler(webapp2.RequestHandler):
    def get(self):        
        user = users.get_current_user()
        if user:
            greeting = ('Your userID is: %s' %
                        (user.user_id()))
        else:
            greeting = ('<a href="%s">Sign in or register to see your userID</a>.' %
                        users.create_login_url('/admin'))

        self.response.out.write('%s' % greeting)

app = webapp2.WSGIApplication([
#    ('/', MainHandler),
    ('/do/addQuote', quoteHandler.QuoteSubmitHandler),
    ('/do/deleteQuote', quoteHandler.QuoteDeleteHandler),
    ('/do/editQuote', quoteHandler.QuoteEditHandler),
    ('/cookie/hide/(.*)', AddCookieHandler),
    ('/cookie/clear', ClearCookieHandler),
    ('/admin', AdminHandler),
    ('/(.*)', MainHandler)
], debug=True)

