from google.appengine.ext import ndb
import logging
from random import randint
import datetime


class QuoteOfTheDay(ndb.Model):
    changeTime = ndb.DateTimeProperty()
    quoteKey = ndb.KeyProperty(required=True) # 500 Characters MAX
    
    @classmethod
    def getDailyQuote(cls):
        dailyQuote = cls.query().fetch()
        if len(dailyQuote) > 1 or (len(dailyQuote) > 0 and dailyQuote[0].changeTime < datetime.datetime.now()):
            for quote in dailyQuote:
                quote.key.delete()
            dailyQuote = []
        if len(dailyQuote) == 0:
            allQuotes = Quote.query().fetch()
            randomQuote = randint(0,len(allQuotes)-1)
            for quote in allQuotes:
                if randomQuote == 0:
                    newDailyQuote = QuoteOfTheDay(quoteKey = quote.key,
                                                  changeTime = datetime.datetime.now() + datetime.timedelta(days=1))
                    newDailyQuote.put()
                    return quote
                else:
                    randomQuote = randomQuote-1
        return dailyQuote[0].quoteKey.get()

class Quote(ndb.Model):
    quote = ndb.StringProperty(indexed=True, required=True) # 500 Characters MAX
    author = ndb.StringProperty(indexed=True, required=True) # 500 Characters MAX
    createdTime = ndb.DateTimeProperty(auto_now_add=True, indexed=True)
    userID = ndb.StringProperty(indexed=True, required=True)

    @classmethod
    def quoteForCategory(cls, ancestor_key):
        return cls.query(ancestor=ancestor_key).order(-cls.createdTime)
        
    @classmethod
    def allQuotesWithOrder(cls, type, order, user_id=""):
        if user_id != "":
            quotes = cls.query(cls.userID == user_id)
        else:
            quotes = cls.query()
        if type == "author":
            quotes = quotes.order(-cls.author if order=="down" else +cls.author)
        elif type == "quote":
            quotes = quotes.order(-cls.quote if order=="down" else +cls.quote)
        elif type == "categories":
            quotes = []
            cats = Category.allCategoriesWithOrder(order)
            for category in cats:
                if user_id != "":
                    catQuotes = Quote.query(Quote.userID == user_id, ancestor=category.key).fetch()
                else:
                    catQuotes = Quote.query(ancestor=category.key).fetch()
                for quote in catQuotes:
                    quotes.append(quote)
        else:
            quotes = quotes.order(+cls.createdTime if order=="down" else -cls.createdTime)
        return quotes

class Category(ndb.Model):
    name = ndb.StringProperty(required=True, indexed=True)
    
    @classmethod
    def allCategories(cls):
        return cls.allCategoriesWithOrder("down")
    
    @classmethod
    def allCategoriesWithOrder(cls, order):
        query = cls.query().order(-cls.name if order=="down" else +cls.name)
        if query.count() == 0:
            newCategory = Category(name="Funny")
            newCategory.put()
            newCategory = Category(name="Not Funny")
            newCategory.put()
        return query

class MWInputError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return self.value

def quoteExists(quoteSearch):
    if not quoteSearch:
        return False
    quoteQuery = Quote.query(Quote.quote==quoteSearch)
    if quoteQuery.count() > 0:
        return True
    return False

def getCategoryKey(categorySearch):
    categoryQuery = Category.query(Category.name==categorySearch)
    return categoryQuery.get().key
